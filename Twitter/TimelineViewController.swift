//
//  TimelineViewController.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 11/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit
import TwitterKit
import CoreData

class TimelineViewController: UIViewController, UITableViewDelegate, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var timelineDataSource: TimelineTweetsDataSource!
    var tweetsProvider: TweetsProviding!
    
    var sortByDate = false
    var sortByDateAscending = false
    var sortByTextLength = false
    var sortByTextLengthAscending = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.tweetsProvider = TweetsProvider()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 80.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = self.view.tintColor
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
        self.setUpDataSource()
        self.tweetsProvider.syncTweets({
        })
    }

    func setUpDataSource(){
        let ctx = CoreDataController.sharedController.cdm.managedObjectContext!
        self.timelineDataSource.setUp(self, byDate: self.sortByDate, dateAscending: self.sortByDateAscending, byTextLength: self.sortByTextLength, textLengthAscending: self.sortByTextLengthAscending, ctx: ctx)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
    }
    
    func syncTweets(completionCosure:() -> ()){
        self.tweetsProvider.syncTweets({
            completionCosure()
        })
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        self.syncTweets({
            refreshControl.endRefreshing()
        })
    }
    
    func hideTweet(sender: AnyObject?) {
        guard let hideButton = sender as? UIButton,
            let cell = hideButton.superview?.superview as? UITableViewCell,
            let indexPath = self.tableView.indexPathForCell(cell),
            let tweet = self.timelineDataSource.fetchedResultsController.objectAtIndexPath(indexPath) as? Tweet
            else { return }
        tweet.hidden = true
        CoreDataController.sharedController.cdm.saveContext()
    }

}

//MARK: NSFetchedResultsControllerDelegate
extension TimelineViewController {

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch (type) {
            case .Insert:
                if let newIndexPath = newIndexPath {
                    self.tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
                }
            case .Delete:
                if let indexPath = indexPath {
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                }
            case .Update:
                if let indexPath = indexPath, let cell = self.tableView.cellForRowAtIndexPath(indexPath) as? TweetTableViewCell {
                    self.timelineDataSource.configureCell(cell, indexPath: indexPath)
                }
            case .Move:
                if let indexPath = indexPath, newIndexPath = newIndexPath {
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                    self.tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
                }
        }
    }
}
