//
//  LogInViewControllerSpec.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 11/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//
import Foundation
import Quick
import Nimble

@testable
import Twitter

class LogInViewControllerSpec: QuickSpec {

    override func spec() {

        var sut: LogInViewController!

        beforeEach {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let navigationController = storyboard.instantiateViewControllerWithIdentifier("login") as! UINavigationController
            sut = navigationController.viewControllers.first as! LogInViewController
        }

        afterEach {
            sut = nil
        }

        it("should have a title") {
            expect(sut.title).to(equal("Log in"))
        }
    }

}
