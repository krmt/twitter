//
//  Tweet+CoreDataProperties.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 14/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Tweet {

    @NSManaged var createdAt: NSDate?
    @NSManaged var hidden: NSNumber?
    @NSManaged var text: String?
    @NSManaged var textLength: NSNumber?
    @NSManaged var tweetID: NSNumber?
    @NSManaged var user: String?

}
