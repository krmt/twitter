//
//  CoreDataController.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 12/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit

class CoreDataController: NSObject {
    
    class var sharedController: CoreDataController {
        struct Static {
            static var instance: CoreDataController?
            static var token: dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = CoreDataController()
        }
        return Static.instance!
    }
    
    lazy var cdstore: CoreDataStore = {
        let cdstore = CoreDataStore()
        return cdstore
    }()
    
    lazy var cdm: CoreDataManager = {
        let cdm = CoreDataManager()
        return cdm
    }()
    
}
