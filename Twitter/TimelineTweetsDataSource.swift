//
//  TimelineTweetsDataSource.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 12/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit
import TwitterKit
import CoreData

let tweetTableReuseIdentifier = "tweetCell"

class TimelineTweetsDataSource: NSObject, UITableViewDataSource {
    
    var fetchedResultsController: NSFetchedResultsController!
    lazy var dateFormatter: NSDateFormatter = {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEE MMM dd HH:mm:ss yyyy"
        formatter.timeZone = NSTimeZone(name: "GMT")
        return formatter
    }()

    func setUp(dlg: NSFetchedResultsControllerDelegate?, byDate:Bool, dateAscending:Bool, byTextLength:Bool, textLengthAscending: Bool, ctx:NSManagedObjectContext){
        let fReq = NSFetchRequest(entityName: "Tweet")
        let predicate = NSPredicate(format: "hidden == false")
        var sorters = [NSSortDescriptor]()
        if byDate {
            sorters.append(NSSortDescriptor(key: "createdAt", ascending: dateAscending))
        }
        if byTextLength {
            sorters.append(NSSortDescriptor(key: "textLength", ascending: textLengthAscending))
        }
        fReq.predicate = predicate
        fReq.sortDescriptors = sorters
        self.fetchedResultsController = nil
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: fReq, managedObjectContext: ctx, sectionNameKeyPath: nil, cacheName: nil)
        if let dlg = dlg {
            self.fetchedResultsController.delegate = dlg
        }

        do {
            try self.fetchedResultsController.performFetch()
        } catch (let fetchError) {
            print(fetchError)
        }
    }

}

//MARK: UITableViewDataSource
extension TimelineTweetsDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier(tweetTableReuseIdentifier, forIndexPath: indexPath) as? TweetTableViewCell else {return UITableViewCell()}
        self.configureCell(cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.fetchedResultsController.sections, let sectionInfo = sections[section] as NSFetchedResultsSectionInfo?
            else {return 0}
        return sectionInfo.numberOfObjects
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard let sections = self.fetchedResultsController.sections
            else {return 0}
        return sections.count
    }
    
    func configureCell(cell: TweetTableViewCell, indexPath: NSIndexPath) {
        guard let tweet = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Tweet else { return }
        cell.hideButton.addTarget(self.fetchedResultsController.delegate, action: Selector("hideTweet:"), forControlEvents: .TouchUpInside)
        let attributedString = NSMutableAttributedString()
        attributedString.appendAttributedString(NSAttributedString(string: "\(self.dateFormatter.stringFromDate(tweet.createdAt!))\n", attributes:[NSFontAttributeName:UIFont.systemFontOfSize(10.0)]))
        if let user = tweet.user {
            attributedString.appendAttributedString(NSAttributedString(string: "@\(user)\n", attributes:[NSFontAttributeName:UIFont.systemFontOfSize(12.0, weight: 2.0)]))
        }
        if let text = tweet.text {
            attributedString.appendAttributedString(NSAttributedString(string: text, attributes:[NSFontAttributeName:UIFont.systemFontOfSize(12.0)]))
        }
        cell.tweetText.attributedText = attributedString
    }
}




