//
//  TimelineTweetsDataSourceSpec.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 14/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import Foundation
import Quick
import Nimble
import CoreData

@testable
import Twitter

class TimelineTweetsDataSourceSpec: QuickSpec {

    override func spec() {
        describe("TimelineTweetsDataSource") {

            var sut: TimelineTweetsDataSource!

            beforeEach {
                sut = TimelineTweetsDataSource()
            }

            afterEach {
                sut = nil
            }

            it("should exist") {
                expect(sut).notTo(beNil())
            }

            describe("tweets sorting") {

                var itemOne: Tweet!
                var itemTwo: Tweet!
                var ctx: NSManagedObjectContext!

                beforeEach {
                    ctx = CoreDataController.sharedController.cdm.setUpInMemoryManagedObjectContext()
                    Tweet.tweetWith(["id":0,"created_at":"Tue Aug 28 21:16:23 +0000 2012","text":"just another test","user":["name": "sample user one"]], moc: ctx)
                    Tweet.tweetWith(["id":1,"created_at":"Tue Aug 27 21:16:23 +0000 2012","text":"just test","user":["name": "sample user two"]], moc: ctx)
                    do {
                        try ctx.save()
                    } catch {
                    }
                }

                afterEach{
                    itemOne = nil
                    itemTwo = nil
                }

                describe("by date descending") {

                    beforeEach {
                        sut.setUp(nil, byDate: true, dateAscending: false, byTextLength: false, textLengthAscending: false, ctx: ctx)
                        do {
                            try sut.fetchedResultsController.performFetch()
                        } catch (let fetchError) {
                            print(fetchError)
                        }
                        itemOne = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 0, inSection:0)) as! Tweet
                        itemTwo = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 1, inSection:0)) as! Tweet
                    }

                    it("should sort tweets accordingly") {
                        expect(itemOne.createdAt!.timeIntervalSince1970) >= itemTwo.createdAt!.timeIntervalSince1970
                    }
                }

                describe("by date ascending") {

                    beforeEach {
                        sut.setUp(nil, byDate: true, dateAscending: true, byTextLength: false, textLengthAscending: false, ctx: ctx)
                        do {
                            try sut.fetchedResultsController.performFetch()
                        } catch (let fetchError) {
                            print(fetchError)
                        }
                        itemOne = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 0, inSection:0)) as! Tweet
                        itemTwo = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 1, inSection:0)) as! Tweet
                    }

                    it("should sort tweets accordingly") {
                        expect(itemOne.createdAt!.timeIntervalSince1970) <= itemTwo.createdAt!.timeIntervalSince1970
                    }
                }

                describe("by text length descending") {

                    beforeEach {
                        sut.setUp(nil, byDate: false, dateAscending: false, byTextLength: true, textLengthAscending: false, ctx: ctx)
                        do {
                            try sut.fetchedResultsController.performFetch()
                        } catch (let fetchError) {
                            print(fetchError)
                        }
                        itemOne = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 0, inSection:0)) as! Tweet
                        itemTwo = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 1, inSection:0)) as! Tweet
                    }

                    it("should sort tweets accordingly") {
                        expect(itemOne.textLength!.integerValue) >= itemTwo.textLength!.integerValue
                    }
                }
                
                describe("by text length ascending") {

                    beforeEach {
                        sut.setUp(nil, byDate: false, dateAscending: false, byTextLength: true, textLengthAscending: true, ctx: ctx)
                        do {
                            try sut.fetchedResultsController.performFetch()
                        } catch (let fetchError) {
                            print(fetchError)
                        }
                        itemOne = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 0, inSection:0)) as! Tweet
                        itemTwo = sut.fetchedResultsController.objectAtIndexPath(NSIndexPath(forRow: 1, inSection:0)) as! Tweet
                    }
                    
                    it("should sort tweets accordingly") {
                        expect(itemOne.textLength!.integerValue) <= itemTwo.textLength!.integerValue
                    }
                }

            }

        }
    }
}
