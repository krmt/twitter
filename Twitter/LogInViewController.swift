//
//  LogInViewController.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 11/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit
import TwitterKit

class LogInViewController: UIViewController {

    var logInButton: TWTRLogInButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logInButton = TWTRLogInButton {(session, error) in
            if let error = error {
                let alert = UIAlertController(title: "Login error", message: error.localizedDescription, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                })
            } else {
                self.activityIndicator.stopAnimating()
                if let unwarappedSession = session {
                    NSUserDefaults.standardUserDefaults().setObject(unwarappedSession.authToken, forKey: "authToken")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    self.performSegueWithIdentifier("showTimeline", sender: self)
                } else {
                    let alert = UIAlertController(title: "Login error", message: error!.localizedDescription, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
        logInButton.addTarget(self, action: Selector("logInClicked"), forControlEvents: .TouchUpInside)
        logInButton.frame = CGRectMake(20, self.view.frame.size.height-88, self.view.frame.size.width-40, 44)
        self.view.addSubview(logInButton)
    }
    
    func logInClicked() {
        self.activityIndicator.startAnimating()
        self.activityIndicator.hidden = false
    }
}