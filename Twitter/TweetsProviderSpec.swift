//
//  TweetsProviderSpec.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 14/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable
import Twitter

class TweetsProviderSpec: QuickSpec {
    override func spec() {
        var sut: TweetsProvider!

        beforeEach {
            sut = TweetsProvider()
        }

        afterEach {
            sut = nil
        }

        describe("reloading tweets") {
            it("should call completion when done") {
                waitUntil(timeout: 5) { done in
                    sut.syncTweets({
                        done()
                    })
                }
            }
        }
    }
}
