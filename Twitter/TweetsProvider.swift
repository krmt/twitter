//
//  TweetsProvider.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 12/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit
import TwitterKit

protocol TweetsProviding {
    func syncTweets(completionClosure: () -> ())
}

class TweetsProvider: NSObject, TweetsProviding {
    
    func getBaseURL() -> String? {
        if let path = NSBundle.mainBundle().pathForResource("URLs", ofType: "plist"), dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            return dict["url"] as? String
        }
        return nil
    }
    
    func syncTweets(completionClosure:() -> ()) {
        
        guard let url = getBaseURL(), let userID = Twitter.sharedInstance().sessionStore.session()?.userID else { return }

        let client = TWTRAPIClient(userID: userID)
        let homeTimeline = url + "statuses/home_timeline.json"
        var clientError : NSError?
        let request = client.URLRequestWithMethod("GET", URL: homeTimeline, parameters: ["count": "50"], error: &clientError)
        client.sendTwitterRequest(request) { (response, data, error ) -> Void in
            do {
                if let data = data {
                    if let tweets: [[String: AnyObject]] = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [[String : AnyObject]] {
                        let ctx = CoreDataController.sharedController.cdm.managedObjectContext!
                        for tweet in tweets {
                            Tweet.tweetWith(tweet, moc: ctx)
                        }
                        CoreDataController.sharedController.cdm.saveContext(ctx)
                    }
                }
                completionClosure()
            } catch (let jsonError) {
                print(jsonError)
                completionClosure()
            }
        }
    }
    
}
