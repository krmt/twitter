//
//  FiltersViewController.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 13/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit

class FiltersViewController: UITableViewController {
    
    @IBOutlet weak var sortByDateSwitch: UISwitch!
    @IBOutlet weak var sortDateAscendingSwitch: UISwitch!
    @IBOutlet weak var sortByTextLengthSwitch: UISwitch!
    @IBOutlet weak var sortByTextLengthAscendingSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let timeline = (self.navigationController?.presentingViewController as? UINavigationController)?.topViewController as? TimelineViewController {
            self.sortByDateSwitch.setOn(timeline.sortByDate, animated: false)
            if timeline.sortByDate {
                self.sortDateAscendingSwitch.enabled = true
                self.sortDateAscendingSwitch.setOn(timeline.sortByDateAscending, animated: false)
            } else {
                self.sortDateAscendingSwitch.enabled = false
                self.sortDateAscendingSwitch.setOn(false, animated: false)
            }
            
            self.sortByTextLengthSwitch.setOn(timeline.sortByTextLength, animated: false)
            if timeline.sortByTextLength {
                self.sortByTextLengthAscendingSwitch.enabled = true
                self.sortByTextLengthAscendingSwitch.setOn(timeline.sortByTextLengthAscending, animated: false)
            } else {
                self.sortByTextLengthAscendingSwitch.enabled = false
                self.sortByTextLengthAscendingSwitch.setOn(false, animated: false)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func save(sender: AnyObject) {
        if let timeline = (self.navigationController?.presentingViewController as? UINavigationController)?.topViewController as? TimelineViewController {
                self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: {
                timeline.tableView.reloadData()
                timeline.setUpDataSource()
            })
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func setSwitchValue(sender: UISwitch) {
        guard let timeline = (self.navigationController?.presentingViewController as? UINavigationController)?.topViewController as? TimelineViewController else { return }
        switch sender.tag {
            case 0:
                timeline.sortByDate = sender.on
                if !timeline.sortByDate {
                    self.sortDateAscendingSwitch.setOn(false, animated: true)
                    self.sortDateAscendingSwitch.enabled = false
                } else {
                    self.sortDateAscendingSwitch.enabled = true
                }
            case 1:
                timeline.sortByDateAscending = sender.on
            case 2:
                timeline.sortByTextLength = sender.on
                if !timeline.sortByTextLength {
                    self.sortByTextLengthAscendingSwitch.setOn(false, animated: true)
                    self.sortByTextLengthAscendingSwitch.enabled = false
                } else {
                    self.sortByTextLengthAscendingSwitch.enabled = true
            }
            case 3:
                timeline.sortByTextLengthAscending = sender.on
            default:
                break
        }
    }
}