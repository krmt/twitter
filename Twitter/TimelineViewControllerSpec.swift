//
//  TimelineViewControllerSpec.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 14/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable
import Twitter

class TimelineViewControllerSpec: QuickSpec {

    override func spec() {

        var sut: TimelineViewController!

        beforeEach {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let navigationController = storyboard.instantiateViewControllerWithIdentifier("timeline") as! UINavigationController
            sut = navigationController.viewControllers.first as! TimelineViewController
        }

        afterEach {
            sut = nil
        }
        
        it("should have a title") {
            expect(sut.title).to(equal("Home"))
        }

        describe("setup tableView dataSource") {

            beforeEach {
                sut.setUpDataSource()
            }

            it("should exist") {
                expect(sut.timelineDataSource).notTo(beNil())
            }

            it("should be an instance of an TimelineTweetsDataSource") {
                expect(sut.timelineDataSource).to(beAnInstanceOf(TimelineTweetsDataSource))
            }
        }

        describe("sync") {

            var tweetsProviderFake: TweetsProviderFake!

            beforeEach {
                tweetsProviderFake = TweetsProviderFake()
                sut.tweetsProvider = tweetsProviderFake
            }

            afterEach {
                tweetsProviderFake = nil
            }

            it("should sync tweets") {
                sut.tweetsProvider.syncTweets({})
                expect(tweetsProviderFake.syncTweetsCalled) == true
            }
        }
    }

}
