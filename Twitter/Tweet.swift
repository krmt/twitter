//
//  Tweet.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 12/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import Foundation
import TwitterKit
import CoreData

class Tweet: NSManagedObject {
    
    class func tweetWith(dictionary:[String : AnyObject], moc: NSManagedObjectContext) -> Tweet {
        let identifier = dictionary["id"] as! NSNumber
        var finalTweet: Tweet?
        if let final = Tweet.findTweetWith(identifier, moc: moc) {
            finalTweet = final
        } else {
            finalTweet = NSEntityDescription.insertNewObjectForEntityForName("Tweet", inManagedObjectContext: moc) as? Tweet
            finalTweet?.hidden = NSNumber(bool: false)
        }
        finalTweet?.tweetID = identifier
        if let text = dictionary["text"] as? String {
            finalTweet?.text = text
            finalTweet?.textLength = text.characters.count
        }
        if let user = dictionary["user"], let name = user["name"] {
            finalTweet?.user = name as? String
        }
        let dateFormatter = NSDateFormatter()
        //"Thu Mar 19 22:54:05 +0000 2009"
        dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "GMT")
        if let dateString = dictionary["created_at"] as? String, let date = dateFormatter.dateFromString(dateString) as NSDate? {
            finalTweet?.createdAt = date
        } else {
            finalTweet?.createdAt = NSDate()
        }
        return finalTweet!
    }
    
    class func findTweetWith(tweetID: NSNumber, moc: NSManagedObjectContext) -> Tweet? {
        let fReq: NSFetchRequest = NSFetchRequest(entityName: "Tweet")
        fReq.predicate = NSPredicate(format: "tweetID == %@", tweetID)
        fReq.returnsObjectsAsFaults = false
        do {
            let result = try moc.executeFetchRequest(fReq)
            if result.count != 1 {
                return nil
            }
            return result.first as? Tweet
        } catch (let fetchError) {
            print(fetchError)
            return nil
        }
    }
}


