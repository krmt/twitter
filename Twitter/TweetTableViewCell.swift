//
//  TweetTableViewCell.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 13/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {

    @IBOutlet weak var tweetText: UILabel!
    @IBOutlet weak var hideButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
