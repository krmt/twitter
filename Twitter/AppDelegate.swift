//
//  AppDelegate.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 11/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

import UIKit
import Fabric
import TwitterKit
import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachability: Reachability?
    var reachabilityInfo: UILabel?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Fabric.with([Twitter.self])
        self.setUpInitial()
        self.setUpReachability()
        return true
    }

    func applicationDidEnterBackground(application: UIApplication) {
        CoreDataController.sharedController.cdm.saveContext()
    }
    
    func applicationWillTerminate(application: UIApplication) {
        CoreDataController.sharedController.cdm.saveContext()
    }
}

extension AppDelegate {
    
    func startWith(viewControllerId: String){
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier(viewControllerId)
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
    }
    
    func setUpInitial(){
        if let _ = NSUserDefaults.standardUserDefaults().objectForKey("authToken") {
            self.startWith("timeline")
        } else {
            self.startWith("login")
        }
    }
    
    func setUpReachability(){
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
            self.reachabilityInfo = UILabel(frame: CGRectMake(0, UIScreen.mainScreen().bounds.height-44, UIScreen.mainScreen().bounds.width, 44))
            self.reachabilityInfo?.backgroundColor = UIColor.darkGrayColor()
            self.reachabilityInfo?.textAlignment = .Center
            self.reachabilityInfo?.font = UIFont.systemFontOfSize(12.0)
            self.reachabilityInfo?.textColor = UIColor.whiteColor()
            self.reachabilityInfo?.text = "No network connection"
            reachability?.whenReachable = { reachability in
                dispatch_async(dispatch_get_main_queue()) {
                    UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                        self.reachabilityInfo?.alpha = 0.0
                    }, completion: { finished in
                        self.reachabilityInfo?.removeFromSuperview()
                    })
                }
            }
            reachability?.whenUnreachable = { reachability in
                dispatch_async(dispatch_get_main_queue()) {
                    self.reachabilityInfo?.alpha = 0.0
                    self.window?.addSubview(self.reachabilityInfo!)
                    UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                        self.reachabilityInfo?.alpha = 1.0
                    }, completion: nil)
                }
            }
            do {
                try reachability?.startNotifier()
            } catch {
                print("Unable to start notifier")
            }
        } catch {
            print("Unable to create Reachability")
            return
        }
    }
    
}

