//
//  TweetsProviderFake.swift
//  Twitter
//
//  Created by Konrad Muchowicz on 14/12/15.
//  Copyright © 2015 Perpetuum. All rights reserved.
//

@testable
import Twitter

class TweetsProviderFake: TweetsProviding {
    
    var syncTweetsCalled = false
    var completion: (() -> ())?

    // MARK: TweetsProviding
    func syncTweets(completionClosure:() -> ()) {
        self.syncTweetsCalled = true
        self.completion = completionClosure
    }
}
